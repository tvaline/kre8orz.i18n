/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
package com.kre8orz.i18n.processor;

/**
 * Internationalization and string constant factory. Convenience class used to centralize the location of useful
 * constants.
 *
 * @author  Tom Valine (thomas.valine@gmail.com)
 */
class I18NProcessorConstants {

    //J-

    /** The default catalog class name. */
    static final String DEFAULT_CATALOG_CLASS_NAME = "I18NCatalog" ;

    /** Package separator. */
    static final String DOT_SEPARATOR = "." ;

    /** Empty string. */
    static final String EMPTY_STRING = "" ;

    /** Forward slash. */
    static final String FORWARD_SLASH = "/" ;

    /** The null string. */
    static final String NULLWORD = "null" ;

    /** The output encoding for generated source files. */
    static final String UTF8_ENCODING = "UTF-8" ;

    /** The output encoding for generated resource files. */
    static final String ASCII_ENCODING = "8859_1" ;

    /** Package separator search expression. */
    static final String PKG_SEP_REGEX = "\\." ;

    /** Fully qualified processor class name. */
    static final String PROCESSOR_FQ_CLASS_NAME = I18NProcessor.class.getName();

    /** The property file extension. */
    static final String PROPERTY_FILE_SUFFIX = ".properties" ;

    /** Underscore character. */
    static final String UNDERSCORE = "_" ;
    //J+
}
/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
