/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
/**
 * Used to generate localized resource bundles from the i18n annotations present in your project as well as a catalog
 * class containing a map of interface class types to resource bundle locations. The annotation processor performs two
 * main functions; Namely resource bundle generation and catalog class generation. The package for both the generated
 * bundles as well as the catalog class are fully customizable.
 *
 * <p>Typically the annotation processor would be used as a pre-compile step in a build so that the generated files are
 * available during the compilation step.
 *
 * <p>
 * <center><b>The annotation processor is compatible with Java versions 1.7 or greater.</b></center>
 * <hr/>
 * <h2>Examples</h2>
 *
 * <h3>Build Integration</h3>
 *
 * This example illustrates a typical pre compilation build target. This snippet calls the ant javac task, processing
 * any I18N annotated interfaces in the <tt>src</tt> project subdirectory, and generates the corresponding resource
 * bundles and catalog file in the <tt>gen</tt> project subdirectory.
 * <div style="margin: 0px auto;width:80%">
 * <pre>
        &lt;javac debug="true"
            destdir="gen"
            source="1.7"
            srcdir="src"
            includes="**\/*.java"&gt;
            &lt;classpath&gt;
                &lt;path path="${javac.classpath}"/&gt;
                &lt;path path="${libs.i18n.classpath}"/&gt;
            &lt;/classpath&gt;
            &lt;compilerarg line="-proc:only"/&gt;
            &lt;compilerarg line="-processor com.kre8orz.i18n.annotation.I18NProcessor"/&gt;
            &lt;compilerarg line="-Acom.kre8orz.i18n.I18NProcessor.catalogClass=com.kre8orz.service.i18n.I18NCatalog"/&gt;
        &lt;/javac&gt;
 * </pre>
 * </div>
 *
 * Some key things to note in the target above:
 *
 * <ul>
 *   <li>The <tt>-proc:only</tt> argument specifies that this invocation of javac is for annotation processing only. No
 *     actual compilation will take place.</li>
 *   <li>The <tt>-processor</tt> argument simply lists the fully qualified class name of the annotation processor</li>
 *   <li>The <tt>-Acom.kre8orz.i18n.I18NProcessor.catalogClass</tt> is an option for the annotation processor which
 *     indicates the fully qualified class name to use for the generated catalog class.
 *   </ul>
 *
 *   <h3>Generated Catalog Class</h3>
 *
 *   An example of a generated catalog class is shown below. This class is generated to provide developers a centralized
 *   method of discovering the paths to generated bundles. The generation of this class can be suppressed by specifying
 *   'null' as the catalog class name via the respective processor option. Please see
 *   {@link com.kre8orz.i18n.processor.I18NProcessor} for more information.
 *   <div style="margin:0px auto; width:80%">
 *   <pre>
package com.kre8orz.service.i18n;

import java.util.*;

public final class I18NCatalog {

    private static final Map&lt;Class&lt;?&gt;,String&gt; map;

    public static String getBundlePath(Class&lt;?&gt; clz){
        return map.get(clz);
    }

    static {
        map = new HashMap&lt;Class&lt;?&gt;,String&gt;();
        map.put(com.kre8orz.sys.SystemMsg.class,"com/kre8orz/sys/res/SystemMsg");
    }
}
 *   </pre>
 *   </div>
 */
package com.kre8orz.i18n.processor;
/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
