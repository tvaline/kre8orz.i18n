/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
package com.kre8orz.i18n.processor;

import java.util.*;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.Diagnostic.Kind;

import static com.kre8orz.i18n.processor.I18NProcessorConstants.*;
import static com.kre8orz.i18n.processor.I18NProcessorMessages.*;
import static javax.tools.Diagnostic.Kind.*;

/**
 * Helper class used to parse processor options and encapsulate their values.
 *
 * <p>The following properties are available for processor customization. How these properties are passed to your
 * compiler are vendor specific, however for Oracle Java compilers 1.7.x or higher, they are specified using <tt><span
 * style="white-space:nowrap;">-A&lt;property_name&gt;[=&lt;property_value&gt;]</span></tt></p>
 *
 * <ul>
 *   <li>com.kre8orz.i18n.processor.I18NProcessor.catalogClass - Specifies the fully qualified catalog class name. If
 *     the word <b>null</b> is specified then the catalog class is not generated.</li>
 *   <li>com.kre8orz.i18n.processor.I18NProcessor.obfuscate - Specifies the key phrase to use for obfuscation. If this
 *     option is supplied then the generated bundle messages will be obfuscated. If this option is left empty or is
 *     omitted then no obfuscation is performed.</li>
 *   <li>
 *     <p>com.kre8orz.i18n.processor.I18NProcessor.language - Specifies the logging/message output language. Currently
 *     supported languages are:</p>
 *
 *     <ul>
 *       <li>English (en_US) This is the default.</li>
 *       <li>Spanish (es_ES)</li>
 *     </ul>
 *   </li>
 * </ul>
 *
 * @author  Tom Valine (thomas.valine@gmail.com)
 */
class I18NProcessorOptions {

    //~ Static fields/initializers *************************************************************************************

    /* This is a helper collection that is used to store the valid options
     * supported by the processor. */
    private static final Set<String> _SUPPORTED;

    /* The name of the processor option to set the catalog class name. */
    private static final String _CATALOG_NAME;

    /* The name of the processor option to set the language. */
    private static final String _LANGUAGE;

    /* The name of the processor option to set the verbosity level. */
    private static final String _VERBOSITY;

    /* The name of the processor option to set the verbosity level. */
    private static final String _OBFUSCATE;

    /* Instantiate and populate the supported option storage collection. */
    static {
        _CATALOG_NAME = PROCESSOR_FQ_CLASS_NAME + ".catalogClass" /*NOI18N*/;
        _LANGUAGE = PROCESSOR_FQ_CLASS_NAME + ".language" /*NOI18N*/;
        _VERBOSITY = PROCESSOR_FQ_CLASS_NAME + ".verbosity" /*NOI18N*/;
        _OBFUSCATE = PROCESSOR_FQ_CLASS_NAME + ".obfuscate" /*NOI18N*/;
        _SUPPORTED = new HashSet<String>();
        _SUPPORTED.add(_CATALOG_NAME);
        _SUPPORTED.add(_LANGUAGE);
        _SUPPORTED.add(_VERBOSITY);
        _SUPPORTED.add(_OBFUSCATE);
    }

    //~ Methods ********************************************************************************************************

    /**
     * Returns the generated key used to decode the system messages and templates for the processor. The value is read
     * from a properties file that is generated during the build and is the same key that the build uses to encrypt all
     * the resource bundles used by the processor.
     *
     * @return  The key used to decrypt the processors system messages and templates.
     */
    public static String getKey() {
        String bundle = "com/kre8orz/i18n/processor/c" /*NOI18N*/;
        ResourceBundle rb = ResourceBundle.getBundle(bundle);

        return rb.getString("MSG0001" /*NOI18N*/);
    }

    /**
     * Returns a list of names of supported options.
     *
     * @return  A list of names of supported options.
     */
    static Set<String> getSupportedOptions() {
        return Collections.unmodifiableSet(_SUPPORTED);
    }

    //~ Instance fields ************************************************************************************************

    /* The name of the catalog class. It is populated either by
     * specifiying the processor option to set it, or by being set to the
     * default catalog class name.*/
    private String _catName;

    /* The locale used to by the processor to generate messages.  It is
     * populated either by specifiying the processor option to set it or by being set to the ROOT
     * locale.*/
    private Locale _locale;

    /* Filtering logger used to record messages meeting a minimum urgency
     * requirement. */
    private I18NProcessorMessages _log;

    /* Indicates the obfuscator to use. */
    private I18NObfuscator _obfuscator;

    /* The processing environment supplied by the tool. */
    private ProcessingEnvironment _pe;

    /* Indicates the processors message verbosity level. */
    private Kind _verbosity;

    //~ Constructors ***************************************************************************************************

    /**
     * Creates a new I18NProcessorOptions object. Instantiating this class will result in parsing the provided options
     * and storing their data, making it available to calling code via it's public methods.
     *
     * @param  pe  The processing environment supplied by the tool.
     */
    I18NProcessorOptions(ProcessingEnvironment pe) {
        _pe = pe;
        _processOptions(pe.getOptions());
    }

    //~ Methods ********************************************************************************************************

    /**
     * Returns the name to use in generating the catalog.
     *
     * @return  The name to use in generating the catalog.
     */
    String getCatalogName() {
        return _catName;
    }

    /**
     * Returns the locale to be used in generating messages.
     *
     * @return  The locale to be used in generating messages.
     */
    Locale getLocale() {
        return _locale;
    }

    /**
     * Returns the configured obfuscator which will be used by the processor to obfuscate the message data.
     *
     * @return  The configured obfuscator or <tt>null</tt> if the obfuscation option was not specified.
     */
    I18NObfuscator getObfuscator() {
        return _obfuscator;
    }

    /**
     * Returns the locale to be used in generating messages.
     *
     * @return  The locale to be used in generating messages.
     */
    Kind getVerbosity() {
        return _verbosity;
    }

    /**
     * Indicates whether or not catalog generation should be suppressed.
     *
     * @return  True if catalog generation should be suppressed.
     */
    boolean isSuppressCatalog() {
        return NULLWORD.equalsIgnoreCase(_catName);
    }

    /* Helper method that parses options supplied by the processing environment
     * which is provided by the compiler or processor host. */
    private void _processOptions(Map<String, String> options) {
        String locale = options.get(_LANGUAGE);
        boolean localeError = false;

        if ((locale != null) && !locale.isEmpty()) {
            try {
                String[] parts = locale.split(UNDERSCORE);

                _locale = new Locale(parts[0], parts[1]);
            } catch (Throwable ex) {
                localeError = true;
                _locale = Locale.ROOT;
            }
        } else {
            _locale = Locale.ROOT;
        }

        String verbosity = options.get(_VERBOSITY);
        boolean verbosityError = false;

        if ((verbosity != null) && !verbosity.isEmpty()) {
            try {
                _verbosity = Kind.valueOf(verbosity);
            } catch (Throwable ex) {
                _verbosity = MANDATORY_WARNING;
                verbosityError = true;
            }
        } else {
            _verbosity = MANDATORY_WARNING;
        }

        Messager msgr = _pe.getMessager();

        _log = new I18NProcessorMessages(msgr, _locale, _verbosity);
        if (localeError) {
            _log.format(WARNING, ERROR_SETTING_OPTION, _LANGUAGE, locale);
        }
        if (verbosityError) {
            _log.format(WARNING, ERROR_SETTING_OPTION, _VERBOSITY, verbosity);
        }

        String catalogName = options.get(_CATALOG_NAME);

        if ((catalogName != null) && !catalogName.isEmpty()) {
            _catName = catalogName.trim();
        } else {
            _catName = DEFAULT_CATALOG_CLASS_NAME;
        }
        if (Locale.ROOT.equals(_locale)) {
            _log.format(NOTE, OPTION_SET_TO, _LANGUAGE, null);
        } else {
            _log.format(NOTE, OPTION_SET_TO, _LANGUAGE, _locale);
        }

        String obfuscate = options.get(_OBFUSCATE);

        if ((obfuscate != null) && !obfuscate.isEmpty()) {
            try {
                _obfuscator = new I18NObfuscator(obfuscate);
                _log.format(NOTE, OPTION_SET_TO, _OBFUSCATE, _obfuscator);
            } catch (Throwable ex) {
                _log.format(WARNING, ERROR_SETTING_OPTION, _OBFUSCATE, obfuscate);
            }
        }
        _log.format(NOTE, OPTION_SET_TO, _VERBOSITY, _verbosity);
        _log.format(NOTE, OPTION_SET_TO, _CATALOG_NAME, _catName);
    }
}
/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
