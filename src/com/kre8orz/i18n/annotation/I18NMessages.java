/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
package com.kre8orz.i18n.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * I18NMessage collection annotation. Used to group a set of I18NMessages and bind them to a field. This is typically
 * done when specifying additional annotations for different locales for a particular message.
 *
 * <p>For example:</p>
 *
 * <pre>
   &#64;I18NMessages({
        &#64;I18NMessage(value="Loading service: {0}"),
        &#64;I18NMessage(locale="en_US",value="Loading service: {0}"),
        &#64;I18NMessage(locale="es_ES",value="Cargando servicio: {0}"),
        &#64;I18NMessage(locale="fr_FR",value="Chargement service: {0}"),
    })
    public static final String LOADING_SERVICE = "LOADING_SERVICE";
 * </pre>
 *
 * @author  Tom Valine (thomas.valine@gmail.com)
 */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.FIELD)
public @interface I18NMessages {

    //~ Instance fields ////////////////////////////////////////////////////////////////////////////////////////////////

    /** The collection of individual annotations. */
    I18NMessage[] value();
}
/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
