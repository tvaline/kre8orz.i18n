/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
/**
 * Used to associate internationalization information with the <tt>String</tt> constant representation of the keys for
 * your bundle key interface as well as associating bundle specific information with the interface type itself.
 * <hr/>
 * <h2>Examples</h2>
 *
 * This example illustrates a typical bundle key interface design.
 * <div style="margin: 0px auto;width:80%">
 * <pre>
package com.yourcompany.i18n;

import com.kre8orz.i18n.annotation.I18NMessage;
import com.kre8orz.i18n.annotation.I18NMessages;
import com.kre8orz.i18n.annotation.I18NResourceBundle;



&#64;I18NResourceBundle(
    bundleName="SystemMsgResources",
    packageName="com.yourcompany.i18n.res"
)
public interface SystemMsg {
    &#64;I18NMessages({
        &#64;I18NMessage(value="Loading service: {0}"),
        &#64;I18NMessage(locale="en_US",value="Loading service: {0}"),
        &#64;I18NMessage(locale="es_ES",value="Cargando servicio: {0}"),
        &#64;I18NMessage(locale="fr_FR",value="Chargement service: {0}"),
    })
    public static final String LOADING_SERVICE = "LOADING_SERVICE";

    &#64;I18NMessages({
        &#64;I18NMessage(value="Unloading service: {0}"),
        &#64;I18NMessage(locale="en_US",value="Unloading service: {0}"),
        &#64;I18NMessage(locale="es_ES",value="Descarga servicio: {0}"),
        &#64;I18NMessage(locale="fr_FR",value="Déchargement service: {0}"),
    })
    public static final String UNLOADING_SERVICE = "UNLOADING_SERVICE";
}
 * </pre>
 * </div>
 *
 * Note that the {@link com.kre8orz.i18n.annotation.I18NResourceBundle} annotation provides information about the bundle
 * base name and the package into which it should be generated. Additionally the
 * {@link com.kre8orz.i18n.annotation.I18NMessage} annotation specifies locale specific information about a bundle
 * message. This information includes the actual unformatted message as well as the optional locale setting. If no
 * locale is specified, then the the corresponding message is assumed to belong to the root bundle.
 *
 * <p/>It should be noted that only constant <tt>String</tt> fields should be tagged with an I18NMessage annotation. The
 * annotation processor enforces this requirement and will generate an error if an annotated field does not conform.
 */
package com.kre8orz.i18n.annotation;
/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
