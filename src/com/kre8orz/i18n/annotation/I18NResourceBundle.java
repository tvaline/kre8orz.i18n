/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
package com.kre8orz.i18n.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Resource bundle annotation. Apply this annotation to the interface that contains the bundle key constants for your
 * messages. This bundle if specified is used to customize both the generated resource bundle base name as well as the
 * package in which the resource bundles should be generated.
 *
 * <p>If this annotation is not applied bundles are generated in the same package as the class that contained the <tt>
 * I18NMessage</tt> annotations. Similarly the base name for the generated resource bundle is the simple name of the
 * class that contained the <tt>I18NMessage</tt> annotations.</p>
 *
 * <p>For example:</p>
 *
 * <pre>
   &#64;I18NResourceBundle(
       bundleName="MessageKeysResources",
       packageName="com.yourcompany.i18n.res"
   )
   public interface MessageKeys {
     &hellip;
   }
 * </pre>
 *
 * @author  Tom Valine (thomas.valine@gmail.com)
 */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE})
public @interface I18NResourceBundle {

    //~ Instance fields ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * The unqualified bundle basename. This should be the simple name of the root bundle; That is the bundle that has
     * no trailing locale in it's filename. (e.g. MyMessages).
     */
    String bundleName();

    /** The fully qualified package name that should contain the generated resource bundles. */
    String packageName();
}
/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
