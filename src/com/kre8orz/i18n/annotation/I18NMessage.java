/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
package com.kre8orz.i18n.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used to annotate interface fields with internationalized message information. The value of the annotation should be
 * the unformatted message that will be formatted and localized when used in the application. The locale attribute is
 * optional and indicates which locale specific property file the message should be written into. If no locale is
 * specified, the <tt>Locale.ROOT</tt> locale will be assumed. An optional note can be supplied which will be used as
 * the resource bundle comment for the entry.
 *
 * <p>For example:</p>
 *
 * <pre>
  &#64;I18NMessage(locale="fr_FR",value="Chargement service: {0}"),
  public static final String LOADING_SERVICE = "LOADING_SERVICE";
 *</pre>
 *
 * @author  Tom Valine (thomas.valine@gmail.com)
 */
@Documented
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.FIELD)
public @interface I18NMessage {

    //~ Instance fields ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * The unformatted message. This will be the value component of the key/value pair in the respective generated
     * resource bundle.
     */
    String value();

    /**
     * The optional locale (e.g en_US). If no locale is specified, the <tt>Locale.ROOT</tt> locale is assumed. This
     * indicates which locale specific resource bundle property file the entry should be written to.
     */
    String locale() default "" /*NOI18N*/;

    /**
     * The optional resource bundle comment for this message. This value will be used as the comment for the key/value
     * pair in the respective generated resource bundle. If no note is specified, no comment will be written for the
     * entry.
     */
    String note() default "" /*NOI18N*/;
}
/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
