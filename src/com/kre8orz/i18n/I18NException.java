/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
package com.kre8orz.i18n;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Runtime exception class used to indicate an error occurred while fetching or formatting a localized message. This
 * class is provided as a convenience to developers giving them the opportunity to either catch or defer exceptions of
 * this type. Being a runtime exception, instances of this exception type will continue to be thrown back up the call
 * stack if they are not caught or otherwise handled by calling code.
 *
 * @author  Tom Valine (thomas.valine@gmail.com)
 */
public final class I18NException extends RuntimeException {

    //~ Instance fields ************************************************************************************************

    /* The parameters that were used in the formatting attempt. */
    private Object[] _args;

    /* The unformatted message entry for which formatting was attempted. */
    private String _entry;

    /* The bundle message key used to look up the unformatted message entry. */
    private String _key;

    /* The locale used to obtain and/or format the message. */
    private String _locale;

    //~ Constructors ***************************************************************************************************

    /**
     * Creates a new I18NException object.
     *
     * @param  key     The key used to look up the message in the target bundle.
     * @param  entry   The unformatted message entry for which formatting was attempted.
     * @param  locale  The locale specified for the message lookup.
     * @param  cause   The underlying exception cause.
     * @param  args    The parameters with which message formatting was attempted.
     */
    public I18NException(String key, String entry, String locale, Throwable cause, Object... args) {
        super(cause);
        _key = key;
        _entry = entry;
        _locale = locale;
        _args = args;
    }

    //~ Methods ********************************************************************************************************

    /**
     * Returns the parameters with which message formatting was attempted.
     *
     * @return  The parameters with which message formatting was attempted.
     */
    public Collection<Object> getArgs() {
        return Collections.unmodifiableCollection(Arrays.asList(_args));
    }

    /**
     * Returns the unformatted message entry for which formatting was attempted.
     *
     * @return  The unformatted message entry for which formatting was attempted.
     */
    public String getEntry() {
        return _entry;
    }

    /**
     * The key used to look up the message in the target bundle.
     *
     * @return  The key used to look up the message in the target bundle.
     */
    public String getKey() {
        return _key;
    }

    /**
     * The locale specified for the message lookup.
     *
     * @return  The locale specified for the message lookup.
     */
    public String getLocale() {
        return _locale;
    }

    /**
     * Returns a string representation of this exception. Particularly, the result consists of the key, entry, locale,
     * cause, and any additional parameters recorded by the exception, printed in array notation.
     *
     * @return  A string representation of this exception.
     *
     * @see     java.lang.Throwable
     */
    @Override
    public String toString() {
        List<Object> params = new ArrayList<Object>();

        params.add(_key);
        params.add(_entry);
        params.add(_locale);
        params.add(getCause().getMessage());
        params.addAll(Arrays.asList(_args));
        return params.toString();
    }
}
/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
