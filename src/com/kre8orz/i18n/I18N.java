/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
package com.kre8orz.i18n;

import com.kre8orz.i18n.processor.I18NObfuscator;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Localized I18N message factory. This class is the main class used by calling code to obtain and format messages in a
 * specific locale. It is essentially a lightweight wrapper around the <tt>ResourceBundle</tt> class and it is assumed
 * that developers using this class and API are familiar with the Java resource bundle API.
 *
 * @author  Tom Valine (thomas.valine@gmail.com)
 * @see     java.util.ResourceBundle
 */
public class I18N {

    //~ Instance fields ************************************************************************************************

    /*  The obfuscator to use. */
    private I18NObfuscator _obfuscator;

    /* The underlying resource bundle used to obtain unformatted messages. */
    private ResourceBundle _rb;

    //~ Constructors ***************************************************************************************************

    /**
     * Creates a new I18N object using the <tt>Locale.ROOT</tt> locale. The new instance will obtain messages in the
     * ROOT locale. The root locale corresponds to the unlocalized bundle entries or those entries in the bundle
     * property file not having a locale suffix. For example, if a bundle having the name <tt>BundleMessages</tt> has a
     * <tt>en_US</tt> locale specified, running on a JVM where the default locale is <tt>en_US</tt> the ROOT locale
     * would be the messages specified in the BundleMessages.properties and NOT BundleMessages_en_US.properties. If a
     * user instead wanted to use the default locale then the alternate form of this method taking a locale parameter,
     * should be used. The resulting instance will assume that the messages are not obfuscated.
     *
     * @param  bundleName  The name of the bundle to retrieve
     *
     * @see    I18N#I18N(java.lang.String, java.util.Locale)
     */
    public I18N(String bundleName) {
        this(bundleName, null);
    }

    /**
     * Creates a new I18N object using the specified locale. If the locale is null, the new instance will obtain
     * messages in the ROOT locale. The root locale corresponds to the unlocalized bundle entries or those entries in
     * the bundle property file not having a locale suffix. For example, if a bundle having the name <tt>
     * BundleMessages</tt> has a <tt>en_US</tt> locale specified, running on a JVM where the default locale is <tt>
     * en_US</tt> the ROOT locale would be the messages specified in the BundleMessages.properties and NOT
     * BundleMessages_en_US.properties. The resulting instance will assume that the messages are not obfuscated.
     *
     * @param  bundleName  The name of the bundle to retrieve.
     * @param  locale      The default bundle to use. The root locale will be used if this value is null.
     */
    public I18N(String bundleName, Locale locale) {
        this(bundleName, locale, null);
    }

    /**
     * Creates a new I18N object using the specified locale. If the locale is null, the new instance will obtain
     * messages in the ROOT locale. The root locale corresponds to the unlocalized bundle entries or those entries in
     * the bundle property file not having a locale suffix. For example, if a bundle having the name <tt>
     * BundleMessages</tt> has a <tt>en_US</tt> locale specified, running on a JVM where the default locale is <tt>
     * en_US</tt> the ROOT locale would be the messages specified in the BundleMessages.properties and NOT
     * BundleMessages_en_US.properties.
     *
     * @param  bundleName  The name of the bundle to retrieve.
     * @param  locale      The default bundle to use. The root locale will be used if this value is null.
     * @param  keyPhrase   The key phrase to use for de-obfuscation. If this is <tt>null</tt> or empty de-obfuscation is
     *                     not performed. If specified, this value must match the key phrase that was used to obfuscate
     *                     the messages.
     */
    public I18N(String bundleName, Locale locale, String keyPhrase) {
        if (locale == null) {
            locale = Locale.ROOT;
        }
        if (keyPhrase != null) {
            if (!keyPhrase.isEmpty()) {
                _obfuscator = new I18NObfuscator(keyPhrase);
            }
        } else {
            _obfuscator = null;
        }
        _rb = ResourceBundle.getBundle(bundleName, locale);
    }

    //~ Methods ********************************************************************************************************

    /**
     * Returns a formatted localized message.
     *
     * @param   key  The message key.
     *
     * @return  A formatted localized message.
     *
     * @throws  I18NException  If an error occurs.
     *
     * @see     I18N#get(java.lang.String, java.lang.Object[])
     */
    public String get(String key) throws I18NException {
        return get(key, new Object[] {});
    }

    /**
     * Returns a formatted localized message.
     *
     * @param   key   The message key.
     * @param   args  The formatting parameters.
     *
     * @return  A formatted localized message.
     *
     * @throws  I18NException  If an error occurs.
     *
     * @see     ResourceBundle#getString(java.lang.String)
     * @see     MessageFormat#format(java.lang.String, java.lang.Object[])
     */
    public String get(String key, Object... args) throws I18NException {
        String entry = null;

        try {
            entry = _rb.getString(key);
            if (_obfuscator != null) {
                entry = _obfuscator.unobfuscate(entry);
            }
            if ((args != null) && (args.length > 0)) {
                entry = MessageFormat.format(entry, args);
            }
            return entry;
        } catch (Throwable e) {
            String locale = _rb.getLocale().toString();

            throw new I18NException(key, entry, locale, e, args);
        }
    }
}
/* Copyright Tom Valine 2002,2014 All Rights Reserved. ****************************************************************/
