import com.kre8orz.i18n.I18N;
import com.kre8orz.i18n.annotation.I18NMessage;
import com.kre8orz.i18n.annotation.I18NMessages;
import com.kre8orz.i18n.annotation.I18NResourceBundle;
import java.util.Locale;

@I18NResourceBundle(bundleName = "ObfuscationTestMessages", packageName = "foo")
public interface ObfuscationTestMain {

    @I18NMessages(
        {
            @I18NMessage(value = "default"),
            @I18NMessage(
                locale = "en_US", value = "en_US"),
            @I18NMessage(locale = "fr_FR", value = "fr_FR"),
            @I18NMessage(locale = "ja_JP", value = "日本")
        })
    String MSG001 = "MSG001";

    public static class Main implements ObfuscationTestMain {

        public static void main(String[] args) {
            I18N defalt = new I18N("foo/ObfuscationTestMessages", Locale.ROOT, "obfuscate");
            I18N us = new I18N("foo/ObfuscationTestMessages", Locale.US, "obfuscate");
            I18N fr = new I18N("foo/ObfuscationTestMessages", Locale.FRANCE, "obfuscate");
            I18N jp = new I18N("foo/ObfuscationTestMessages", Locale.JAPAN, "obfuscate");
            String defVal = defalt.get(MSG001);
            String usVal = us.get(MSG001);
            String frVal = fr.get(MSG001);
            String jpVal = jp.get(MSG001);

            if (!"default".equals(defVal)) {
                throw new RuntimeException();
            }

            if (!"en_US".equals(usVal)) {
                throw new RuntimeException();
            }

            if (!"fr_FR".equals(frVal)) {
                throw new RuntimeException();
            }

            String expected = "日本";

            if (!expected.equals(jpVal)) {
                throw new RuntimeException();
            }
        }
    }
}
