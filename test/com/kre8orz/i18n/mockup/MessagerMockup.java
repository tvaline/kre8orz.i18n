package com.kre8orz.i18n.mockup;

import java.util.EnumMap;
import java.util.Map;
import javax.annotation.processing.Messager;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic.Kind;

public class MessagerMockup implements Messager {

    Map<Kind, Integer> counts = new EnumMap<Kind, Integer>(Kind.class);

    public MessagerMockup() {

        for (final Kind kind : Kind.values()) {
            counts.put(kind, 0);
        }
    }

    public Integer getCount(Kind kind) {
        return counts.get(kind);
    }

    @Override
    public void printMessage(Kind kind, CharSequence msg) {
        record(kind);
    }

    @Override
    public void printMessage(Kind kind, CharSequence msg, Element e) {
        record(kind);
    }

    @Override
    public void printMessage(Kind kind, CharSequence msg, Element e, AnnotationMirror a) {
        record(kind);
    }

    @Override
    public void printMessage(Kind kind, CharSequence msg, Element e, AnnotationMirror a, AnnotationValue v) {
        record(kind);
    }

    private void record(Kind kind) {
        Integer count = counts.get(kind);
        count = (count == null) ? 1 : (count + 1);
        counts.put(kind, count);
    }
}
