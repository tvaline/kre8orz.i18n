package com.kre8orz.i18n.mockup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import javax.tools.SimpleJavaFileObject;

public final class SourceInputFileMockup extends SimpleJavaFileObject {

    private static final String _ext = ".txt";

    private String _data;

    public SourceInputFileMockup(String fqResourcePath) {
        super(calculateUri(fqResourcePath), Kind.SOURCE);
        load(fqResourcePath);
    }

    private static URI calculateUri(String fqResourcePath) {

        if (fqResourcePath.endsWith(_ext)) {
            int extLoc = fqResourcePath.lastIndexOf(_ext);
            String uriPath = fqResourcePath.substring(0, extLoc);

            return URI.create("string://" + uriPath + Kind.SOURCE.extension);
        } else {
            String msg = "Input source files must use a \"%s\" extension";
            throw new IllegalArgumentException(String.format(msg, _ext));
        }
    }

    @Override
    public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
        return _data;
    }

    protected void load(String fqResourcePath) {
        BufferedReader reader = null;
        StringBuilder sb = null;

        try {
            InputStream is = getClass().getResourceAsStream(fqResourcePath);
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

            String line;

            while ((line = reader.readLine()) != null) {

                if (sb == null) {
                    sb = new StringBuilder(line).append("\n");
                } else {
                    sb.append(line).append("\n");
                }
            }

            _data = sb.toString();
        } catch (Throwable ex) {
            throw new RuntimeException(ex);
        } finally {

            if (reader != null) {

                try {
                    reader.close();
                } catch (IOException ex) {
                    /* Can't do anything here. */
                }
            }
        } // end try-catch-finally
    }
}
