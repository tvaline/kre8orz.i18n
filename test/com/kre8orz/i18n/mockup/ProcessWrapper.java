package com.kre8orz.i18n.mockup;

import java.io.BufferedReader;
import java.io.File;
import java.io.StringReader;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

public class ProcessWrapper {

    private String[] command;
    private File cwd;
    private long duration;
    private String[] environment;
    private CountDownLatch gate = new CountDownLatch(1);
    private int retcode;
    private StringBuffer stderr = new StringBuffer();
    private StringBuffer stdout = new StringBuffer();

    public ProcessWrapper(String command) {
        this(command, (String) null);
    }

    public ProcessWrapper(String[] command) {
        this(command, new String[] {});
    }

    public ProcessWrapper(String command, String cwd) {
        this((command == null) ? null : command.split("\\s+"), cwd);
    }

    public ProcessWrapper(String command, String[] env) {
        this((command == null) ? null : command.split("\\s+"), env, null);
    }

    public ProcessWrapper(String[] command, String cwd) {
        this(command, new String[] {}, cwd);
    }

    public ProcessWrapper(String[] command, String[] environment) {
        this(command, environment, null);
    }

    public ProcessWrapper(String[] command, String[] environment, String cwd) {

        if (command == null) {
            throw new IllegalArgumentException("Command cannot be null!");
        }

        this.command = command;

        if ((cwd != null) && (cwd.length() > 0)) {
            this.cwd = new File(cwd);
        } else {
            this.cwd = null;
        }

        if ((environment != null) && (environment.length > 0)) {
            this.environment = environment;
        } else {
            environment = null;
        }
    }

    public String getCommand() {
        StringBuilder sb = new StringBuilder();

        if (command != null) {

            for (final String part : command) {
                sb.append(part).append(" ");
            }
        }

        return sb.toString().trim();
    }

    public String getCWD() {
        return (cwd == null) ? "" : cwd.getAbsolutePath();
    }

    public long getDuration() {
        return duration;
    }

    public String[] getEnvironment() {
        return (environment == null) ? new String[0] : environment;
    }

    public int getRetCode() {
        return retcode;
    }

    public String getStdErr() {
        return stderr.toString();
    }

    public BufferedReader getStdErrReader() {
        return new BufferedReader(new StringReader(getStdErr()));
    }

    public String getStdOut() {
        return stdout.toString();
    }

    public BufferedReader getStdOutReader() {
        return new BufferedReader(new StringReader(getStdOut()));
    }

    public void run() {
        run(0);
    }

    public void run(long timeout) {
        long start = new Date().getTime();
        Thread worker = null;

        try {
            worker = new Thread(new Runnable() {
                    public void run() {
                        Process p = null;

                        try {
                            p = Runtime.getRuntime().exec(command, environment, cwd);

                            StreamReaderThread stdoutThread = new StreamReaderThread(p.getInputStream(), stdout);
                            StreamReaderThread stderrThread = new StreamReaderThread(p.getErrorStream(), stderr);
                            stdoutThread.start();
                            stderrThread.start();
                            stdoutThread.join();
                            stderrThread.join();
                            retcode = p.waitFor();
                        } catch (InterruptedException ex) {
                            stderr.append("Process interrupted after exceeding the timeout");
                            retcode = 1;
                        } catch (Throwable ex) {
                            stderr.append(ex.toString());
                            retcode = 1;
                        } finally {

                            if (p != null) {
                                p.destroy();
                            }

                            gate.countDown();
                        }
                    }
                });
            worker.start();
            worker.join(timeout);
        } catch (Exception ex) {
            stderr.append(ex.toString());
            retcode = 1;
        } finally {

            if (worker != null) {
                worker.interrupt();
            }

            try {
                gate.await();
            } catch (InterruptedException ex) {
                throw new RuntimeException(ex);
            }
        } // end try-catch-finally

        duration = new Date().getTime() - start;
    }
}
