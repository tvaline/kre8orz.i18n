package com.kre8orz.i18n.mockup;

import java.util.Locale;
import java.util.Map;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;

public class ProcessingEnvironmentMockup implements ProcessingEnvironment {

    @Override
    public Elements getElementUtils() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Filer getFiler() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Locale getLocale() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Messager getMessager() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Map<String, String> getOptions() {
        throw new UnsupportedOperationException();
    }

    @Override
    public SourceVersion getSourceVersion() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Types getTypeUtils() {
        throw new UnsupportedOperationException();
    }
}
