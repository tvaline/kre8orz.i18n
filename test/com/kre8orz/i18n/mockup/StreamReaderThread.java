package com.kre8orz.i18n.mockup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class StreamReaderThread extends Thread {

    BufferedReader buffer;
    private final StringBuffer captured;

    public StreamReaderThread(InputStream toCapture, StringBuffer captured) {

        if ((toCapture == null) || (captured == null)) {
            throw new IllegalArgumentException("Input stream cannot be null");
        }

        this.captured = captured;
        this.buffer = new BufferedReader(new InputStreamReader(toCapture));
    }

    @Override
    public void run() {

        try {
            String line;

            while ((line = buffer.readLine()) != null) {
                captured.append(line);
                captured.append("\n");
            }
        } catch (IOException ex) {
            captured.append(ex.toString());
        } finally {

            if (buffer != null) {

                try {
                    buffer.close();
                } catch (IOException ex) {
                    /* Can't do anything now */
                }

                buffer = null;
            }
        }
    }
}
