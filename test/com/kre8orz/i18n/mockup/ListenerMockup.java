package com.kre8orz.i18n.mockup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;
import javax.tools.Diagnostic;
import javax.tools.Diagnostic.Kind;
import javax.tools.DiagnosticListener;
import javax.tools.JavaFileObject;

class ListenerMockup implements DiagnosticListener<JavaFileObject> {

    List<Diagnostic<? extends JavaFileObject>> data;

    ListenerMockup() {
        this.data = new ArrayList<Diagnostic<? extends JavaFileObject>>();
    }

    @Override
    public void report(Diagnostic<? extends JavaFileObject> diagnostic) {
        data.add(diagnostic);
    }

    List<Diagnostic<? extends JavaFileObject>> getData() {
        return Collections.unmodifiableList(data);
    }

    boolean hasErrors() {
        boolean result = false;

        for (Diagnostic<? extends JavaFileObject> diagnostic : data) {

            if (Kind.ERROR.equals(diagnostic.getKind())) {
                result = true;

                break;
            }
        }

        return result;
    }

    void printDiagnostics() {
        Logger logger = Logger.getLogger(getClass().getName());
        String msg = "[%s, %s.%d:%d, %s]";

        for (Diagnostic<? extends JavaFileObject> diagnostic : data) {
            long col = diagnostic.getColumnNumber();
            String kind = diagnostic.getKind().name();
            long line = diagnostic.getLineNumber();
            String message = diagnostic.getMessage(null);
            JavaFileObject fObj = diagnostic.getSource();
            String file;

            if (fObj != null) {
                file = fObj.toUri().toString();
            } else {
                file = "";
            }

            logger.warning(String.format(msg, kind, file, line, col, message));
        }
    }
}
