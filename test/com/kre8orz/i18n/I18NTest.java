package com.kre8orz.i18n;

import java.io.IOException;
import java.util.Locale;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class I18NTest {

    @Test
    public void testExceptionGetters() {
        Object[] args = new String[] { "a", "b", "c" };
        Throwable cx = new RuntimeException("foo");
        I18NException ex;
        ex = new I18NException("key", "entry", Locale.ROOT.toString(), cx, args);
        assertArrayEquals(args, ex.getArgs().toArray());
        assertEquals("entry", ex.getEntry());
        assertEquals("key", ex.getKey());
        assertEquals(Locale.ROOT.toString(), ex.getLocale());
        assertEquals(cx, ex.getCause());

        String toString = ex.toString();
        assertTrue(toString.contains("key"));
        assertTrue(toString.contains("entry"));
        assertTrue(toString.contains(Locale.ROOT.toString()));

        for (Object object : args) {
            assertTrue(toString.contains(object.toString()));
        }
    }

    @Test(expected = I18NException.class)
    public void testFormattingException() {
        String bundle = "com/kre8orz/i18n/res/I18NTest";
        I18N i18n = new I18N(bundle, Locale.ROOT, "");
        i18n.get(bundle, new Object(), Boolean.FALSE);
    }

    @Test
    public void testGet() throws IOException {
        String bundle = "com/kre8orz/i18n/res/I18NTest";
        I18N i18n = new I18N(bundle);
        assertEquals("amBkgfQYAmI=", i18n.get("MSG"));
    }

    @Test
    public void testGetLocale() throws IOException {
        String bundle = "com/kre8orz/i18n/res/I18NTest";
        I18N i18n = new I18N(bundle, Locale.ROOT);
        assertEquals("amBkgfQYAmI=", i18n.get("MSG"));
    }

    @Test
    public void testGetLocaleEmptyKeyPhrase() throws IOException {
        String bundle = "com/kre8orz/i18n/res/I18NTest";
        I18N i18n = new I18N(bundle, Locale.ROOT, "");
        assertEquals("amBkgfQYAmI=", i18n.get("MSG"));
    }

    @Test
    public void testObfuscatedGet() throws IOException {
        String bundle = "com/kre8orz/i18n/res/I18NTest";
        String keyPhrase = "Uwe238MFKjwnRHNaYAyR";
        Locale root = Locale.ROOT;
        Locale en = new Locale("en");
        Locale en_US = new Locale("en", "us");
        Locale fr_FR = new Locale("fr", "fr");
        I18N i18n = new I18N(bundle, root, keyPhrase);
        assertEquals("default", i18n.get("MSG"));
        i18n = new I18N(bundle, en, keyPhrase);
        assertEquals("en", i18n.get("MSG"));
        i18n = new I18N(bundle, en_US, keyPhrase);
        assertEquals("en_us", i18n.get("MSG"));
        i18n = new I18N(bundle, fr_FR, keyPhrase);
        assertEquals("fr", i18n.get("MSG"));
    }
}
