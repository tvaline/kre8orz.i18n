package com.kre8orz.i18n.processor;

import com.kre8orz.i18n.annotation.I18NMessage;
import com.kre8orz.i18n.mockup.ProcessorMockup;
import java.io.IOException;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.lang.model.SourceVersion;
import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class I18NProcessorTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @BeforeClass
    public static void beforeClass() {

        if (!Logger.getRootLogger().getAllAppenders().hasMoreElements()) {
            Logger root = Logger.getRootLogger();
            Appender appender = new ConsoleAppender(new SimpleLayout());
            root.addAppender(appender);
            root.setLevel(Level.ERROR);
        }
    }

    @Test
    public void testGetSupportedAnnotationTypes() throws ClassNotFoundException {
        I18NProcessor proc = new I18NProcessor();
        Set<String> types = proc.getSupportedAnnotationTypes();
        Package expectedPkg = I18NMessage.class.getPackage();

        for (final String type : types) {
            Class<?> clazz = Class.forName(type);
            Retention retention = clazz.getAnnotation(Retention.class);
            assertNotNull(retention);

            Documented documented = clazz.getAnnotation(Documented.class);
            assertNotNull(documented);
            assertEquals(RetentionPolicy.SOURCE, retention.value());
            assertTrue(clazz.isAnnotation());
            assertTrue(clazz.isInterface());
            assertEquals(expectedPkg, clazz.getPackage());
        }
    }

    @Test
    public void testGetSupportedOptions() {
        I18NProcessor proc = new I18NProcessor();
        String prefix = I18NProcessor.class.getCanonicalName() + ".";
        Set<String> options = proc.getSupportedOptions();

        for (String option : options) {
            assertTrue(option.startsWith(prefix));
            option = option.substring(prefix.length());

            Pattern pattern = Pattern.compile("[a-z]\\w+");
            Matcher matcher = pattern.matcher(option);
            assertTrue(matcher.matches());
        }
    }

    @Test
    public void testGetSupportedSourceVersion() {
        I18NProcessor proc = new I18NProcessor();
        assertEquals(SourceVersion.latest(), proc.getSupportedSourceVersion());
    }

    @Test
    public void testInvalidModifiers() throws IOException {
        String tmpDir = folder.getRoot().getAbsolutePath();
        List<String> options = new ArrayList<String>();
        ProcessorMockup harness = new ProcessorMockup(tmpDir, options);
        String file = "/com/kre8orz/i18n/processor/res/InvalidFieldModifiers.txt";
        harness.process(file);
    }

    @Test
    public void testObfuscation() throws IOException {
        String tmpDir = folder.getRoot().getAbsolutePath();
        List<String> options = new ArrayList<String>();
        options.add("-Acom.kre8orz.i18n.processor.I18NProcessor.obfuscate=Uwe238MFKjwnRHNaYAyR");

        ProcessorMockup harness = new ProcessorMockup(tmpDir, options);
        String file = "/com/kre8orz/i18n/processor/res/ValidSimpleSourceObfuscate.txt";
        harness.process(file);
    }

    @Test
    public void testProcess() throws IOException {
        String tmpDir = folder.getRoot().getAbsolutePath();
        List<String> options = new ArrayList<String>();
        ProcessorMockup harness = new ProcessorMockup(tmpDir, options);
        String file = "/com/kre8orz/i18n/processor/res/ValidSimpleSource.txt";
        harness.process(file);
    }

    @Test
    public void testSingleAnnotation() throws IOException {
        String tmpDir = folder.getRoot().getAbsolutePath();
        List<String> options = new ArrayList<String>();
        ProcessorMockup harness = new ProcessorMockup(tmpDir, options);
        String file = "/com/kre8orz/i18n/processor/res/ValidSingleMessage.txt";
        harness.process(file);
    }

    @Test
    public void testUnicodeProperty() throws IOException {
        String tmpDir = folder.getRoot().getAbsolutePath();
        List<String> options = new ArrayList<String>();
        options.add("-Acom.kre8orz.i18n.processor.I18NProcessor.catalogClass=null");

        ProcessorMockup harness = new ProcessorMockup(tmpDir, options);
        String file = "/com/kre8orz/i18n/processor/res/ValidUnicodeMessage.txt";
        harness.process(file);
    }

    @Test
    public void testValidMessageComment() throws IOException {
        String tmpDir = folder.getRoot().getAbsolutePath();
        List<String> options = new ArrayList<String>();
        ProcessorMockup harness = new ProcessorMockup(tmpDir, options);
        String file = "/com/kre8orz/i18n/processor/res/ValidMessageComment.txt";
        harness.process(file);
    }

    @Test
    public void testValidPackageNames() throws IOException {
        String tmpDir = folder.getRoot().getAbsolutePath();
        List<String> options = new ArrayList<String>();
        ProcessorMockup harness = new ProcessorMockup(tmpDir, options);
        String file = "/com/kre8orz/i18n/processor/res/ValidCatalogPackageName.txt";
        harness.process(file);
    }
}
