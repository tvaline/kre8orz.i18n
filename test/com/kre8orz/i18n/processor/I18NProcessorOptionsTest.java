package com.kre8orz.i18n.processor;

import com.kre8orz.i18n.mockup.MessagerMockup;
import com.kre8orz.i18n.mockup.ProcessingEnvironmentMockup;
import static com.kre8orz.i18n.processor.I18NProcessorConstants.DEFAULT_CATALOG_CLASS_NAME;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.Diagnostic.Kind;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class I18NProcessorOptionsTest {

    @Test
    public void testInvalidOptions() {
        I18NProcessorOptions options;
        String prefix = I18NProcessor.class.getName();
        final Map<String, String> args = new HashMap<String, String>();
        ProcessingEnvironment pe = new ProcessingEnvironmentMockup() {
                @Override
                public Map<String, String> getOptions() {
                    return args;
                }

                @Override
                public Messager getMessager() {
                    return new MessagerMockup();
                }
            };

        String badData = Long.toHexString(new Date().getTime());
        args.put(prefix + ".catalogClass", null);
        args.put(prefix + ".language", null);
        args.put(prefix + ".verbosity", null);
        options = new I18NProcessorOptions(pe);

        Locale expectedLocale = Locale.ROOT;
        String expectedCatalogName = DEFAULT_CATALOG_CLASS_NAME;
        boolean expectedCatalogSuppression = false;
        Kind expectedVerbosity = Kind.MANDATORY_WARNING;
        assertEquals(expectedLocale, options.getLocale());
        assertEquals(expectedCatalogName, options.getCatalogName());
        assertEquals(expectedCatalogSuppression, options.isSuppressCatalog());
        assertEquals(expectedVerbosity, options.getVerbosity());
        args.clear();
        args.put(prefix + ".catalogClass", "");
        args.put(prefix + ".language", "");
        args.put(prefix + ".verbosity", "");
        options = new I18NProcessorOptions(pe);
        assertEquals(expectedLocale, options.getLocale());
        assertEquals(expectedCatalogName, options.getCatalogName());
        assertEquals(expectedCatalogSuppression, options.isSuppressCatalog());
        assertEquals(expectedVerbosity, options.getVerbosity());
        args.clear();
        args.put(prefix + ".catalogClass", null);
        args.put(prefix + ".language", badData);
        args.put(prefix + ".verbosity", badData);
        options = new I18NProcessorOptions(pe);
        assertEquals(expectedLocale, options.getLocale());
        assertEquals(expectedCatalogName, options.getCatalogName());
        assertEquals(expectedCatalogSuppression, options.isSuppressCatalog());
        assertEquals(expectedVerbosity, options.getVerbosity());
    }

    @Test
    public void testValidOptions() throws ClassNotFoundException {
        I18NProcessorOptions options = null;
        String prefix = I18NProcessor.class.getName();
        final Map<String, String> args = new HashMap<String, String>();
        args.put(prefix + ".catalogClass", "null");
        args.put(prefix + ".language", "en_US");
        args.put(prefix + ".verbosity", "ERROR");

        ProcessingEnvironment pe = new ProcessingEnvironmentMockup() {
                @Override
                public Map<String, String> getOptions() {
                    return args;
                }

                @Override
                public Messager getMessager() {
                    return new MessagerMockup();
                }
            };
        options = new I18NProcessorOptions(pe);

        Locale expectedLocale = new Locale("en", "US");
        String expectedCatalogName = "null";
        boolean expectedCatalogSuppression = true;
        Kind expectedVerbosity = Kind.ERROR;
        assertEquals(expectedLocale, options.getLocale());
        assertEquals(expectedCatalogName, options.getCatalogName());
        assertEquals(expectedCatalogSuppression, options.isSuppressCatalog());
        assertEquals(expectedVerbosity, options.getVerbosity());
    }
}
