package com.kre8orz.i18n.processor;

import static org.junit.Assert.fail;
import org.junit.Test;

public class I18NObfuscatorTest {

    @Test(expected = RuntimeException.class)
    public void testBadConstructor() {
        I18NObfuscator badInstance = new I18NObfuscator(null);
    }

    @Test(expected = RuntimeException.class)
    public void testEmptyKeyphrase() {
        I18NObfuscator badInstance = new I18NObfuscator("");
    }

    @Test
    public void testKeyPhraseLength() {
        I18NObfuscator i18n = new I18NObfuscator("AAAAAAAA");

        try {
            i18n = new I18NObfuscator("AAAAAAA");
        } catch (RuntimeException ex) {
            return;
        }

        fail("DES Key should be a minimum of 8 characters.");
    }

    @Test(expected = RuntimeException.class)
    public void testObfuscate() {
        I18NObfuscator i18n = new I18NObfuscator("AAAAAAAA");
        i18n.obfuscate(null);
    }

    @Test(expected = RuntimeException.class)
    public void testUnobfuscate() {
        I18NObfuscator i18n = new I18NObfuscator("AAAAAAAA");
        i18n.unobfuscate(null);
    }
}

