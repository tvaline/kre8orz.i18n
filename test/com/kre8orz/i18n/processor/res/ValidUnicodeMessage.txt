import com.kre8orz.i18n.annotation.*;
import com.kre8orz.i18n.*;
import java.util.*;

@I18NResourceBundle(bundleName="ValidUnicodeMessages",packageName="com.mycompany")
public class ValidUnicodeMessage {

    @I18NMessages({
        @I18NMessage(value="ó")
    })
    public static final String MSG = "MSG";
    
    public static class Main {
        public static void main(String[] args) {
            ResourceBundle rb;
            String msg;
            rb = ResourceBundle.getBundle("com/mycompany/ValidUnicodeMessages",Locale.ROOT);
            msg = rb.getString(MSG);
            String expected = "\u00f3";
            System.out.println(expected);
            if(!Arrays.equals(expected.getBytes(),msg.getBytes())){
                throw new RuntimeException(msg);
            }
        }
    }   
}   
