package com.kre8orz.i18n.processor;

import com.kre8orz.i18n.mockup.MessagerMockup;
import static com.kre8orz.i18n.processor.I18NProcessorMessages.CANNOT_OPEN_BUNDLE;
import java.util.Locale;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic.Kind;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class I18NProcessorMessagesTest {

    @Test
    public void testFormattingMethods() {
        MessagerMockup host = new MessagerMockup();
        I18NProcessorMessages msgs = new I18NProcessorMessages(host, Locale.ROOT, Kind.ERROR);
        Element ele = null;
        Object[] args = new String[] { "foo", "bar" };
        String note = I18NProcessorMessages.OPTION_SET_TO;
        String msg = msgs.format(Kind.ERROR, note, ele, args);

        for (final Object word : args) {
            assertTrue(msg.contains(word.toString()));
        }

        AnnotationMirror aMirror = null;
        msg = msgs.format(Kind.ERROR, note, ele, aMirror, args);

        for (final Object word : args) {
            assertTrue(msg.contains(word.toString()));
        }

        AnnotationValue aValue = null;
        msg = msgs.format(Kind.ERROR, note, ele, aMirror, aValue, args);

        for (final Object word : args) {
            assertTrue(msg.contains(word.toString()));
        }
    }

    @Test
    public void testMessageFiltering() {
        AnnotationMirror aMirror = null;
        AnnotationValue aValue = null;
        Element ele = null;
        Object[] args = null;

        for (Kind kind : Kind.values()) {
            MessagerMockup host = new MessagerMockup();
            I18NProcessorMessages msgs = new I18NProcessorMessages(host, Locale.ROOT, kind);

            for (Kind msgKind : Kind.values()) {
                msgs.format(msgKind, CANNOT_OPEN_BUNDLE, args);
                msgs.format(msgKind, CANNOT_OPEN_BUNDLE, ele, args);
                msgs.format(msgKind, CANNOT_OPEN_BUNDLE, ele, aMirror, args);
                msgs.format(msgKind, CANNOT_OPEN_BUNDLE, ele, aMirror, aValue, args);
                msgs.record(msgKind, CANNOT_OPEN_BUNDLE);
                msgs.record(msgKind, CANNOT_OPEN_BUNDLE, ele);
                msgs.record(msgKind, CANNOT_OPEN_BUNDLE, ele, aMirror);
                msgs.record(msgKind, CANNOT_OPEN_BUNDLE, ele, aMirror, aValue);
            }

            for (Kind msgKind : Kind.values()) {

                if (msgKind.ordinal() <= kind.ordinal()) {
                    assertTrue(host.getCount(msgKind) == 8);
                } else {
                    assertTrue(host.getCount(msgKind) == 0);
                }
            }
        }
    }

    @Test
    public void testRecordingMethods() {
        MessagerMockup host = new MessagerMockup();
        I18NProcessorMessages msgs = new I18NProcessorMessages(host, Locale.ROOT, Kind.ERROR);
        Element ele = null;
        String note = I18NProcessorMessages.OPTION_SET_TO;
        String msg;
        msgs.record(Kind.ERROR, note);
        assertTrue(host.getCount(Kind.ERROR) == 1);
        host = new MessagerMockup();
        msgs = new I18NProcessorMessages(host, Locale.ROOT, Kind.ERROR);
        msgs.record(Kind.ERROR, note, ele);
        assertTrue(host.getCount(Kind.ERROR) == 1);
        host = new MessagerMockup();
        msgs = new I18NProcessorMessages(host, Locale.ROOT, Kind.ERROR);

        AnnotationMirror aMirror = null;
        msgs.record(Kind.ERROR, note, ele, aMirror);
        assertTrue(host.getCount(Kind.ERROR) == 1);
        host = new MessagerMockup();
        msgs = new I18NProcessorMessages(host, Locale.ROOT, Kind.ERROR);

        AnnotationValue aValue = null;
        msgs.record(Kind.ERROR, note, ele, aMirror, aValue);
        assertTrue(host.getCount(Kind.ERROR) == 1);
    }
}
